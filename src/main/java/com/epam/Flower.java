package com.epam;

public class Flower {
private int price;
private String type;
private String kind;
private int count;
private int id;



    public Flower() {
    }

    public Flower(int price, String type, String kind, int count, int id) {
        this.price = price;
        this.type = type;
        this.kind = kind;
        this.count = count;
        this.id = id;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return  kind + ":  \t count = " + count + "\t| price = " + price + " \t| id = " + id +" | type = " + type ;





    }
}
