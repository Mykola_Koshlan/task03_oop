package com.epam;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

public class Shop {
    public static Flower flowerArr(int id, ArrayList<Flower> vac) {
        Iterator<Flower> myItr = vac.iterator();
        while (myItr.hasNext()) {
            Flower t = myItr.next();
            if (t.getId() == id) {
                return t;
            }
        }
        return null;
    }

    public static void main(String[] args) {
        int id = 0;
        ArrayList<Flower> flowers = new ArrayList<>();
        Flower rose = new Flower();
        rose.setType("Flower");
        rose.setKind("Roses");
        rose.setCount(10);
        rose.setPrice(25);
        rose.setId(id);
        id++;
        flowers.add(rose);
        Flower tulip = new Flower();
        tulip.setType("Flower");
        tulip.setKind("Tulips");
        tulip.setCount(10);
        tulip.setPrice(16);
        tulip.setId(id);
        id++;
        flowers.add(tulip);
        Flower flower1 = new Flower();
        flower1.setType("Flower");
        flower1.setKind("Peonies");
        flower1.setCount(10);
        flower1.setPrice(22);
        flower1.setId(id);
        id++;
        flowers.add(flower1);
        Flower flower2 = new Flower();
        flower2.setType("Flower");
        flower2.setKind("Daisies");
        flower2.setCount(10);
        flower2.setPrice(11);
        flower2.setId(id);
        id++;
        flowers.add(flower2);
        Flower flower3 = new Flower();
        flower3.setType("Flower");
        flower3.setKind("Lilies");
        flower3.setCount(10);
        flower3.setPrice(30);
        flower3.setId(id);
        id++;
        flowers.add(flower3);
        Flower flowerpot1 = new Flower();
        flowerpot1.setType("Flowerpot");
        flowerpot1.setKind("Cactus");
        flowerpot1.setCount(5);
        flowerpot1.setPrice(100);
        flowerpot1.setId(id);
        id++;
        flowers.add(flowerpot1);
        Flower flowerpot2 = new Flower();
        flowerpot2.setType("Flowerpot");
        flowerpot2.setKind("Orchid");
        flowerpot2.setCount(7);
        flowerpot2.setPrice(350);
        flowerpot2.setId(id);
        id++;
        flowers.add(flowerpot2);
        try (Scanner scan = new Scanner(System.in)) {
            System.out.println("FLOWERS FROM ANABEL");
            System.out.println("1 - user");
            System.out.println("2 - admin");
            switch (scan.nextInt()) {
                case 1:
                    Iterator<Flower> myItr1 = flowers.iterator();
                    while (myItr1.hasNext()) {
                        Flower t = myItr1.next();
                        System.out.println(t.toString());
                    }
                    System.out.println("1 - just to buy some flowers");
                    System.out.println("2 - create a bouquet by some parameter");
                    switch (scan.nextInt()) {
                        case 1:
                            System.out.println("Input id of flower which you want: ");
                            id = scan.nextInt();
                            Flower temp = flowerArr(id, flowers);
                            if (temp != null) {
                                System.out.println("How many " + temp.getKind() + " you want to buy?");
                                int count = scan.nextInt();
                                temp.setCount(temp.getCount() - count);
                                int price = temp.getPrice() * count;
                                System.out.println("You need to pay: " + price + "grn");
                            } else {
                                System.err.println("There is now such kind of flowers!");
                            }
                            break;
                        case 2:
                            System.out.println("Parameter:\n" +
                                    "1 - price\n" +
                                    "2 - count\n");
                            if (scan.nextInt() == 1) {
                                System.out.println("Input price: ");
                                int price = scan.nextInt();
                                System.out.println("For " + price + " grn you can buy:");
                                Iterator<Flower> myItr = flowers.iterator();
                                int counter = 0;
                                while (myItr.hasNext()) {

                                    int n = 0;
                                    Flower flower = flowerArr(counter, flowers);
                                    if (flower != null) {
                                        n = price / flower.getPrice();
                                        System.out.println(counter + 1 + ")" + n + " - " + flower.getKind());
                                        counter++;
                                    } else {
                                        break;
                                    }
                                }
                                System.out.println("Choose your bouquet");
                                counter = scan.nextInt() - 1;
                                Flower flower = flowerArr(counter, flowers);
                                if (flower != null) {
                                    flower.setCount(flower.getCount() - (price / flower.getPrice()));
                                }
                            }
                            if(scan.nextInt()==2){
                                System.out.println("Input count: ");
                                int count = scan.nextInt();
                                System.out.println("You can buy:");
                                Iterator<Flower> myItr = flowers.iterator();
                                int counter = 0;
                                while (myItr.hasNext()) {
                                    Flower flower = flowerArr(counter, flowers);
                                    if (flower != null) {
                                        if(flower.getCount()>count) {
                                            System.out.println(counter + 1 + ")" + count + " - " + flower.getKind());
                                        }
                                        counter++;
                                    } else {
                                        break;
                                    }
                                }
                                System.out.println("Choose your bouquet");
                                counter = scan.nextInt() - 1;
                                Flower flower = flowerArr(counter, flowers);
                                if (flower != null) {
                                    flower.setCount(flower.getCount() - count);
                                }
                             }
                            break;
                    }
            }

            Iterator<Flower> myItr1 = flowers.iterator();
            while (myItr1.hasNext()) {
                Flower t = myItr1.next();
                System.out.println(t.toString());
            }
        }
    }


}


